package foxtrobot;

/**
 *
 * @author Kim
 */
public class FoxtroBot {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Config c = new Config();
        c.URL = "irc.freenode.net";
        IRCBot bot = new IRCBot(c);
        bot.connect();
        bot.joinChannel("#medisim");
        bot.mainLoop();
        
    }
    
}
