package foxtrobot;

import foxtrobot.commands.DieCommand;
import foxtrobot.commands.InviteCommand;
import foxtrobot.commands.PartCommand;
import foxtrobot.commands.SeenCommand;
import foxtrobot.commands.SlapCommand;
import foxtrobot.commands.UrlCommand;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zhoot
 */
public class IRCBot {

    public Config config;
    protected long _born = -1;

    protected ArrayList<String> _channels = new ArrayList<>();
    protected ArrayList<String> _ops = new ArrayList<>();
    protected ArrayList<Command> _commands = new ArrayList<>();
    protected HashMap<String, ArrayList<User>> _userMemory = new HashMap<>();

    private Socket con = null;
    private DataInputStream dis = null;
    private PrintStream os = null;
    private BufferedReader br = null;
    private boolean run = true;

    public IRCBot(Config config) {
        this._born = System.currentTimeMillis();
        this.config = config;
    }

    private void reset() {
        this._channels.clear();
        this._ops.clear();
    }

    private void close() {
        try {
            this.con.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void connect() {
        this.reset();
        int tries = 0;
        int maxTries = 5;
        int waittime = 1000;

        do {
            try {
                System.out.println("Connecting " + this.config.URL + ":" + this.config.port + "(" + tries + ")");
                this.con = new Socket(this.config.URL, this.config.port);
            } catch (IOException e) {
                tries++;
                try {
                    Thread.sleep(waittime);
                } catch (InterruptedException ie) {

                }
                waittime *= 2;
            }
        } while (!this.con.isConnected() && tries < maxTries);

        if (this.con.isConnected()) {
            initializeProtocol();
        }
    }

    public void initializeProtocol() {
        try {
            System.out.println("Connected.\nEstablishing Protocol.");

            this.dis = new DataInputStream(this.con.getInputStream());
            this.os = new PrintStream(this.con.getOutputStream());
            this.br = new BufferedReader(new InputStreamReader(this.dis));

            tellServer("NICK " + this.config.nick + "\n");
            tellServer("USER " + this.config.ident + " " + this.config.URL + " bla :" + this.config.realname + "\n");

            registerCommands();
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    protected void registerCommands() {
        this._commands.add(new InviteCommand(this));
        //this._commands.add(new IsLiveCommand(this));
        this._commands.add(new UrlCommand(this));
        this._commands.add(new SeenCommand(this));
        this._commands.add(new DieCommand(this));
        this._commands.add(new PartCommand(this));
        this._commands.add(new SlapCommand(this));
    }

    public void tellServer(String msg) {
        if (!msg.endsWith("\n")) {
            msg += "\n";
        }
        try {
            this.os.println(msg);
            System.out.println("> " + msg);
        } catch (Exception e) {
        }
    }

    public void tellAllChannels(String msg) {
        for (Iterator<String> i = this._channels.iterator(); i.hasNext();) {
            String next = i.next();
            tellServer("PRIVMSG " + next + " :" + msg);
        }
    }

    public void joinChannel(String chn) {
        if (!this._channels.contains(chn)) {
            tellServer("JOIN " + chn);
            this._channels.add(chn);
        }
    }
    
    public void leaveChannel(String chn){
        if(this._channels.contains(chn)){
            tellServer("PART " + chn);
            this._channels.remove(chn);
        }
    }

    public void rememberUsers(Message m) {
        if (!m.channel.contains("#")) {
            return;
        }

        if (!this._userMemory.containsKey(m.channel)) {
            this._userMemory.put(m.channel, new ArrayList<User>());
        } else {
            ArrayList<User> test = this._userMemory.get(m.channel);

            int userIndex = -1;

            for (int i = 0; i < test.size(); i++) {
                if (test.get(i).name.equalsIgnoreCase(m.sender)) {
                    userIndex = i;
                    break;
                }
            }

            if (userIndex == -1) {
                this._userMemory.get(m.channel).add(new User(m.sender));
            } else {
                test.get(userIndex).lastSeen = System.currentTimeMillis();
            }
        }
    }

    public User findUser(String channel, String name) {
        if (!this._userMemory.containsKey(channel)) {
            return null;
        }
        User u = null;
        ArrayList<User> test = this._userMemory.get(channel);

        for (int i = 0; i < test.size(); i++) {
            if (test.get(i).name.equalsIgnoreCase(name)) {
                u = test.get(i);
                break;
            }
        }

        return u;
    }
    
    public void stopRunning(){
        this.run = false;
        
        try {
            this.con.close();
        } catch (IOException ex) {
            Logger.getLogger(IRCBot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mainLoop() {
        String line = "", tmp[];

        for (int i = 0; i < this._channels.size(); i++) {
            tellServer("JOIN " + this._channels.get(i));
        }

        while (this.run) {
            try {
                if ((line = this.br.readLine()) != null) {
                    tmp = line.split(":");
                    System.out.println("< " + line);
                    Message m = new Message(line);
                    System.out.println(m.sender + ", " + m.host + ", " + m.action + ", " + m.channel + ", " + m.message);
                    if (tmp[0].contains("PING")) {
                        tellServer("PONG " + tmp[1]);
                    } else {
                        if (m.validMsg) {
                            rememberUsers(m);
                            for (int a = 0; a < this._commands.size(); a++) {
                                if (this._commands.get(a).check(m)) {
                                    if (this._commands.get(a).needsOP()) {
                                        if (this._commands.get(a).authorized(m)) {
                                            this._commands.get(a).act(m);
                                        } else {
                                            tellServer("PRIVMSG " + m.channel + " :I do not trust you " + m.sender);
                                        }
                                    } else {
                                        this._commands.get(a).act(m);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (IOException e) {
            }
        }
    }
}
