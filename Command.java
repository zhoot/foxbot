package foxtrobot;

/**
 *
 * @author Zhoot
 */
public abstract class Command {
    
    protected IRCBot _bot;
    protected String _cmd;
    protected boolean _opOnly = false;
    
    public Command(IRCBot bot){
        this._bot = bot;
    }
    
    public boolean needsOP(){
        return this._opOnly;
    }
    
    public boolean authorized(Message m){
        return m.sender.equalsIgnoreCase(this._bot.config.owner);
    }
    
    public boolean check(Message m){
        return m.message.startsWith(this._bot.config.prefix + this._cmd);
    }
    
    public abstract void act(Message m);
}