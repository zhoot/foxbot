package foxtrobot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Kim
 */
public class User {
    
    public String name;
    public long lastSeen;
    
    public User(String name){
        this.name = name;
        this.lastSeen = System.currentTimeMillis();
    }
    
    public long getLastSeen(){
        return this.lastSeen;
    }
    
    public void setLastSeen(Long l){
        this.lastSeen = l;
    }
    
    public String getLastSeenAsString(){
        Date date = new Date(this.lastSeen); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+1")); // give a timezone reference for formating (see comment at the bottom
        return sdf.format(date);
    }
}
