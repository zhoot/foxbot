package foxtrobot;

/**
 *
 * @author Zhoot
 */
public class Config {
    
    public int port             = 6667;
    public String URL           = "";
    public String nick          = "Foxtro";
    public String ident         = "Foxtro";
    public String realname      = "Foxtrobot";
    public String prefix        = ".";
    protected String owner      = "Zhoot";
    protected String cmdpass    = "jabba";
    
    public Config(){
        
    }
    
    public String getOwner(){
        return this.owner;
    }
    
    public String getPass() {
        return this.cmdpass;
    }
}
