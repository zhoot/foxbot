package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;
import java.util.Random;

/**
 *
 * @author Zhoot
 */
public class SlapCommand extends Command {

    private String[] items = {"rotten fish", "boot", "floppy", "banana"};
    
    public SlapCommand(IRCBot bot) {
        super(bot);
        this._cmd = "slap";
    }

    @Override
    public void act(Message m) {
        String[] temp = m.message.split(" ");
        if(temp.length >= 2){
            for(int i=1; i < temp.length; i++){
                this._bot.tellServer("PRIVMSG " + m.channel + " :" +m.sender +  " slaps " + temp[i] + " with a " + this.items[new Random().nextInt(this.items.length)]);
            }
        }
    }
    
}
