package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;

/**
 *
 * @author Zhoot
 */
public class UrlCommand extends Command {

    public UrlCommand(IRCBot bot) {
        super(bot);
        this._cmd = "url";
    }

    @Override
    public void act(Message m) {
        String[] temp = m.message.split(" ");

        if (temp.length >= 2) {
            this._bot.tellServer("PRIVMSG " + m.channel + " :http://www.livecoding.tv/" + (temp[1].equalsIgnoreCase("me") ? m.sender : temp[1]));
        }
    }

}
