package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;

/**
 *
 * @author Zhoot
 */
public class PartCommand extends Command {

    public PartCommand(IRCBot bot) {
        super(bot);
        this._cmd = "part";
        this._opOnly = true;
    }

    @Override
    public void act(Message m) {
        String[] temp = m.message.split(" ");

        if (temp.length >= 2) {
            for (int i = 1; i < temp.length; i++) {
                this._bot.leaveChannel(temp[i]);
            }
        } else {
            this._bot.leaveChannel(m.channel);
        }
    }

}
