package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;

/**
 *
 * @author Zhoot
 */
public class InviteCommand extends Command {

    public InviteCommand(IRCBot bot) {
        super(bot);
        this._cmd = "invite";
    }

    @Override
    public void act(Message m) {
        String[] temp = m.message.split(" ");
        if (temp.length >= 2) {
            for (int i = 1; i < temp.length; i++) {
                this._bot.joinChannel(temp[i]);
            }
        }
    }
}
