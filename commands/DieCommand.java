package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;

/**
 *
 * @author Zhoot
 */
public class DieCommand extends Command {

    public DieCommand(IRCBot bot) {
        super(bot);
        this._cmd = "die";
        this._opOnly = true;
    }

    @Override
    public void act(Message m) {
        this._bot.tellServer("PRIVMSG " + m.channel + " :Aaaa!");
        this._bot.tellServer("QUIT");
        try {
            Thread.sleep(1000);
        } catch(InterruptedException e){}
        
        this._bot.stopRunning();
    }
    
}
