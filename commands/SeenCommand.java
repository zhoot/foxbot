package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;
import foxtrobot.User;

/**
 *
 * @author Kim
 */
public class SeenCommand extends Command {

    public SeenCommand(IRCBot bot) {
        super(bot);
        this._cmd = "seen";
    }

    @Override
    public void act(Message m) {
        String[] temp = m.message.split(" ");

        if (temp.length >= 2) {
            String user = temp[1];
            User u = this._bot.findUser(m.channel, user);
            
            if(u != null){
                this._bot.tellServer("PRIVMSG " + m.channel + " :" + user + " was last seen " + u.getLastSeenAsString());
            } else {
                this._bot.tellServer("PRIVMSG " + m.channel + " :I haven't seen " +user + " :(");
            }

        }
    }

}
