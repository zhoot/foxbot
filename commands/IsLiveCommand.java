package foxtrobot.commands;

import foxtrobot.Command;
import foxtrobot.IRCBot;
import foxtrobot.Message;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Zhoot
 */
public class IsLiveCommand extends Command {

    public IsLiveCommand(IRCBot bot) {
        super(bot);
        this._cmd = "islive";
    }

    @Override
    public void act(Message m) {
        String[] temp = m.message.split(" ");
        if (temp.length >= 2) {
            try {
                URL url = new URL("https://www.livecoding.tv/" + temp[1]);

                BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
                boolean found = false;
                
                for (String line; (line = reader.readLine()) != null;) {
                    if(line.contains("Current stream")){
                        this._bot.tellServer("PRIVMSG " + m.channel + " :" + temp[1] + " seem to be online. http://www.livecoding.tv/" + temp[1]);
                        found = true;
                        break;
                    }
                }
                if(!found){
                    this._bot.tellServer("PRIVMSG " + m.channel + " :" + temp[1] + " seem to be offline.");
                }

            } catch (MalformedURLException | UnsupportedEncodingException e) {
                this._bot.tellServer("PRIVMSG " + m.channel + " :I have encountered an error.");
                System.err.println(e.getMessage());
            } catch (IOException e) {
                this._bot.tellServer("PRIVMSG " + m.channel + " :I have encountered an error.");
                System.err.println(e.getMessage());
            }
        } else {
            this._bot.tellServer("PRIVMSG " + m.channel + " :I'm sorry - I didn't understand the query.");
        }
    }

}
