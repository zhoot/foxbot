package foxtrobot;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Kim
 */
public class Message {
    
    public final String PATTERN = "^:([A-Za-z0-9|\\._-]+)!~(?:[A-Za-z0-9|\\._-]+)@([A-Za-z0-9|\\._-]+)\\s([A-Z]+)\\s([A-Za-z0-9|\\.#_-]+)\\s:(.*)$";
    
    public String sender;
    public String host;
    public String action;
    public String channel;
    public String message;
    
    public boolean validMsg = false;
    
    public Message(String raw){
        process(raw);
    }
    
    protected void process(String raw){
        
        ArrayList<String> allMatches = new ArrayList<>();
        Matcher m = Pattern.compile(PATTERN).matcher(raw);

        while(m.find()){
            allMatches.add(m.group());
        }
        
        if(m.matches()){
            this.sender     = m.group(1);
            this.host       = m.group(2);
            this.action     = m.group(3);
            this.channel    = m.group(4);
            this.message    = m.group(5);

            if(sender != null)
                this.validMsg = true;
            }
    }
}
